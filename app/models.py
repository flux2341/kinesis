from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
import pytz
import os


class Category(models.Model):
    name = models.CharField(max_length=20)
    icon = models.CharField(max_length=20, default='')

    def __str__(self):
        return self.name

    def toDictionary(self):
        return {
            'id': self.id,
            'name': self.name,
            'icon': self.icon
        }


class Event(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField(null=True, blank=True)
    location = models.CharField(max_length=500)
    hosts = models.CharField(max_length=500)
    link = models.CharField(max_length=500)
    approved = models.BooleanField()
    category = models.ForeignKey(Category, on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        if not self.approved:
            return 'PENDING - ' + self.title
        return self.title

    def datetime(self):

        if os.name == 'nt':
            dtf = '%A %#m/%#d %#I:%M %p'
            tf = '%#I:%M %p'
        else:
            dtf = '%A %-m/%-d %-I:%M %p'
            tf = '%-I:%M %p'

        sdt = self.start_datetime
        sdt = timezone.localtime(sdt, pytz.timezone('US/Pacific'))

        edt = self.end_datetime
        if edt is None:
            return sdt.strftime(dtf)
        else:
            edt = timezone.localtime(edt, pytz.timezone('US/Pacific'))
            if sdt.year == edt.year \
                and sdt.month == edt.month \
                and sdt.day == edt.day:
                return sdt.strftime(dtf) \
                        + ' - ' + edt.strftime(tf)
            return sdt.strftime(dtf) \
                + ' - ' + edt.strftime(dtf)

    def toDictionary(self):
        
        sdt = self.start_datetime
        sdt = timezone.localtime(sdt, pytz.timezone('US/Pacific'))
        
        edt = self.end_datetime
        if edt is not None:
            edt = timezone.localtime(edt, pytz.timezone('US/Pacific'))
        
        return {
            'title': self.title,
            'description': self.description,
            'hosts': self.hosts,
            'link': self.link,
            'datetime': self.datetime(),
            'start_datetime': sdt.strftime("%m/%d/%Y %H:%M:%S"),
            'end_datetime': '' if edt is None else edt.strftime("%m/%d/%Y %H:%M:%S"),
            'location': self.location
        }



class QueuedFacebookEvent(models.Model):
    url = models.CharField(max_length=200)
    completed = models.BooleanField(default=False)


class Organization(models.Model):
    name = models.CharField(max_length=200)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    url = models.CharField(max_length=200)
    minor_type = models.CharField(max_length=20)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=20)
    email_address = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    def toDictionary(self):
        return {
            'name': self.name,
            'address': self.address,
            'url': self.url,
            'phone_number': self.phone_number,
            'email_address': self.email_address
        }
