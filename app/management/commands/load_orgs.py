
from django.core.management.base import BaseCommand
from app.models import Organization, Category
import csv

class Command(BaseCommand):

    def handle(self, *args, **options):

        # parse csv into a list of dictionaries
        with open('organizations.csv') as file:
            reader = csv.reader(file)
            header = next(reader)
            data = []
            for row in reader:
                data_row = {}
                for i in range(len(header)):
                    data_row[header[i]] = row[i]
                data.append(data_row)

        # delete all organizations
        Organization.objects.all().delete()
        for row in data:
            name = row['Name']
            url = row['Url']
            major_type_name = row['Major Type']
            minor_type_name = row['Minor Type']
            address = row['Address']
            phone_number = row['Phone Number']
            email_address = row['Email Address']
            category, created = Category.objects.get_or_create(name=major_type_name)
            organization = Organization(name=name, url=url, category=category,
                                        minor_type=minor_type_name,
                                        address=address,
                                        phone_number=phone_number,
                                        email_address=email_address)
            organization.save()
