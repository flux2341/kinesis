
from django.urls import path
from . import views

app_name = 'app'

urlpatterns = [
    path('', views.index, name='index'),
    path('get_events/', views.get_events, name='get_events'),
    path('get_orgs/', views.get_orgs, name='get_orgs'),
    path('submit_facebook_event/', views.submit_facebook_event, name='submit_facebook_event'),
    path('pfe/', views.pending_facebook_events, name='pending_facebook_events'),
    path('event_post/', views.event_post, name='event_post')
]
