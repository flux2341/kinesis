

from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from bs4 import BeautifulSoup
import datetime



url_official_animal_rights_march = 'https://www.facebook.com/events/1789125017772768/'
url_orcas = 'https://www.facebook.com/events/290684035043432/'
url_vigil = 'https://www.facebook.com/events/1990929330965802/'
url_spaghetti = 'https://www.facebook.com/events/190891561781432/'
url = url_spaghetti

# binary = FirefoxBinary(r'C:\Program Files\Mozilla Firefox\firefox.exe')
driver = webdriver.Firefox()#firefox_binary=binary)

driver.implicitly_wait(30)
driver.get(url)

#print(driver.page_source)

soup = BeautifulSoup(driver.page_source, 'html.parser')



# title
title = soup.html.head.title.string
print(f'title: {title}\n')

# description
description = soup.find('div', {'data-testid': 'event-permalink-details'})
if description is None:
    description = ''
else:
    description = description.decode_contents()
print(f'description: {description}\n')

# start datetime
# this might be done better, there's an li higher up with id=event_time_info
# this seems to be the only element with a content attribute
datetimes = soup.find('div', {'class': ['_2ycp', '_5xhk']})
datetimes = datetimes['content']

start_datetime = datetimes[:25]
end_datetime = datetimes[29:]
format = "%Y-%m-%dT%H:%M:%S-07:00"
start_datetime = datetime.datetime.strptime(start_datetime, format)
end_datetime = datetime.datetime.strptime(end_datetime, format)

print(f'start_datetime: {start_datetime}\n')
print(f'end_datetime: {end_datetime}\n')

location = soup.find('a', {'id': 'u_0_q'}) # works for vigil
if location is None:
    location = soup.find('span', {'id': 'u_0_k'}) # works for animal rights march
if location is None:
    location = ''
else:
    location = location.text
print(f'location: {location}\n')

organizations = soup.find('div', {'data-testid': 'event_permalink_feature_line'})
organizations = organizations['content']
organizations = organizations.split(' & ')
print(f'organizations: {organizations}\n')


